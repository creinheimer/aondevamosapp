'use strict';

describe('Controller Tests', function() {

    describe('Voto Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockVoto, MockUser, MockRestaurante;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockVoto = jasmine.createSpy('MockVoto');
            MockUser = jasmine.createSpy('MockUser');
            MockRestaurante = jasmine.createSpy('MockRestaurante');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Voto': MockVoto,
                'User': MockUser,
                'Restaurante': MockRestaurante
            };
            createController = function() {
                $injector.get('$controller')("VotoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'aondevamosApp:votoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

package com.aondevamos.web.rest;

//import com.aondevamos.AondevamosApp;

//import com.aondevamos.domain.Voto;
//import com.aondevamos.domain.User;
//import com.aondevamos.domain.Restaurante;
//import com.aondevamos.repository.VotoRepository;
//import com.aondevamos.service.VotoService;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import static org.hamcrest.Matchers.hasItem;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VotoResource REST controller.
 *
 * @see VotoResource
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = AondevamosApp.class)
public class VotoResourceIntTest {


//    @Inject
//    private VotoRepository votoRepository;
//
//    @Inject
//    private VotoService votoService;
//
//    @Inject
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Inject
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Inject
//    private EntityManager em;
//
//    private MockMvc restVotoMockMvc;
//
//    private Voto voto;
//
//    @PostConstruct
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        VotoResource votoResource = new VotoResource();
//        ReflectionTestUtils.setField(votoResource, "votoService", votoService);
//        this.restVotoMockMvc = MockMvcBuilders.standaloneSetup(votoResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static Voto createEntity(EntityManager em) {
//        Voto voto = new Voto();
//        // Add required entity
//        User user = UserResourceIntTest.createEntity(em);
//        em.persist(user);
//        em.flush();
//        voto.setUser(user);
//        // Add required entity
//        Restaurante restaurante = RestauranteResourceIntTest.createEntity(em);
//        em.persist(restaurante);
//        em.flush();
//        voto.setRestaurante(restaurante);
//        return voto;
//    }
//
//    @Before
//    public void initTest() {
//        voto = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createVoto() throws Exception {
//        int databaseSizeBeforeCreate = votoRepository.findAll().size();
//
//        // Create the Voto
//
//        restVotoMockMvc.perform(post("/api/votos")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(voto)))
//                .andExpect(status().isCreated());
//
//        // Validate the Voto in the database
//        List<Voto> votos = votoRepository.findAll();
//        assertThat(votos).hasSize(databaseSizeBeforeCreate + 1);
//        Voto testVoto = votos.get(votos.size() - 1);
//    }
//
//    @Test
//    @Transactional
//    public void checkDataVotoIsRequired() throws Exception {
//        int databaseSizeBeforeTest = votoRepository.findAll().size();
//
//        // Create the Voto, which fails.
//
//        restVotoMockMvc.perform(post("/api/votos")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(voto)))
//                .andExpect(status().isBadRequest());
//
//        List<Voto> votos = votoRepository.findAll();
//        assertThat(votos).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllVotos() throws Exception {
//        // Initialize the database
//        votoRepository.saveAndFlush(voto);
//
//        // Get all the votos
//        restVotoMockMvc.perform(get("/api/votos?sort=id,desc"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//                .andExpect(jsonPath("$.[*].id").value(hasItem(voto.getId().intValue())));
//    }
//
//    @Test
//    @Transactional
//    public void getVoto() throws Exception {
//        // Initialize the database
//        votoRepository.saveAndFlush(voto);
//
//        // Get the voto
//        restVotoMockMvc.perform(get("/api/votos/{id}", voto.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(voto.getId().intValue()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingVoto() throws Exception {
//        // Get the voto
//        restVotoMockMvc.perform(get("/api/votos/{id}", Long.MAX_VALUE))
//                .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateVoto() throws Exception {
//        // Initialize the database
//        votoService.save(voto);
//
//        int databaseSizeBeforeUpdate = votoRepository.findAll().size();
//
//        // Update the voto
//        Voto updatedVoto = votoRepository.findOne(voto.getId());
//
//        restVotoMockMvc.perform(put("/api/votos")
//                .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                .content(TestUtil.convertObjectToJsonBytes(updatedVoto)))
//                .andExpect(status().isOk());
//
//        // Validate the Voto in the database
//        List<Voto> votos = votoRepository.findAll();
//        assertThat(votos).hasSize(databaseSizeBeforeUpdate);
//        Voto testVoto = votos.get(votos.size() - 1);
//    }
//
//    @Test
//    @Transactional
//    public void deleteVoto() throws Exception {
//        // Initialize the database
//        votoService.save(voto);
//
//        int databaseSizeBeforeDelete = votoRepository.findAll().size();
//
//        // Get the voto
//        restVotoMockMvc.perform(delete("/api/votos/{id}", voto.getId())
//                .accept(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk());
//
//        // Validate the database is empty
//        List<Voto> votos = votoRepository.findAll();
//        assertThat(votos).hasSize(databaseSizeBeforeDelete - 1);
//    }
    
}

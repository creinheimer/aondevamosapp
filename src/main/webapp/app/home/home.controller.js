(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Maps', '$compile', 'Voto', 'Grupo'];

    function HomeController ($scope, Principal, LoginService, $state, Maps, $compile, Voto, Grupo) {
        
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.votar = votar;
        vm.removerVoto = removerVoto;
        vm.updateBoard = updateBoard;
        vm.position = {};
        
        vm.photos = [];
        vm.places = [];
        
        vm.voto = {};
        vm.grupos = Grupo.query();
        vm.votos = Voto.count();
        vm.grupo = null;
        
        $scope.$on('authenticationSuccess', function() {
            getAccount();
            vm.voto = Voto.getFromUser();
        });

        getAccount();
        
        vm.loadingMap = true;
        
        Maps.location().then(function(position){
            
            vm.position = position;
            
            var GoogleAPI = google;

            var googlePosition = new GoogleAPI.maps.LatLng(position);
          
            var map = new GoogleAPI.maps.Map(document.getElementById('map'), {
                center: googlePosition,
                zoom: 15
            });
            
            var service = new GoogleAPI.maps.places.PlacesService(map);
            var infowindow = new GoogleAPI.maps.InfoWindow();

            GoogleAPI.maps.event.addListenerOnce(map, 'tilesloaded', function(){
                $scope.$apply(function(){ vm.loadingMap = false; });
            });

            var request = {
                location: googlePosition,
                radius: 10000,
                keyword: "restaurant",
                language: "pt-BR",
                //openNow: true,
                types: ['food|restaurant']
            };
            
            var markersArray = [];
            
            var input = document.getElementById('pac-input');
            var searchBox = new GoogleAPI.maps.places.SearchBox(input);
            map.controls[GoogleAPI.maps.ControlPosition.TOP_LEFT].push(input);

            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', function () {
                var results = searchBox.getPlaces();
                vm.places = results;
                if (results.length == 0) return;
                var bounds = new GoogleAPI.maps.LatLngBounds();
                clearOverlays();
                angular.forEach(results, function(place, i){
                    createMarker(place, i);
                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            service.nearbySearch(request, function (results, status) {
                if (status === GoogleAPI.maps.places.PlacesServiceStatus.OK) {
                    vm.places = results;
                    clearOverlays();
                    angular.forEach(results, function(place, i){
                        createMarker(place, i);
                    });
                }
            });
            
            function clearOverlays() {
                for (var i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }
            
            function createMarker(place, index){
                
                var marker = new GoogleAPI.maps.Marker({
                    position: place.geometry.location,
                    title: place.name,
                    map: map
                });
                
                markersArray.push(marker);

                var content = '<div><p class="lead">'+ place.name +'</p><p ng-show="!vm.isAuthenticated()" data-translate="aondevamosApp.voto.loginToVote"></p><p ng-show="vm.grupo == null && vm.voto.id == null && vm.isAuthenticated()" data-translate="aondevamosApp.voto.chooseYourTeam"></p><p ng-hide="vm.voto.id == null || !vm.isAuthenticated()" data-translate="aondevamosApp.voto.alreadyVoted"></p><button ng-show="vm.voto.id == null" ng-disabled="vm.grupo == null" ng-click="vm.votar('+ index +')" data-translate="aondevamosApp.voto.votar" style="width: 100%;" class="btn btn-success">Votar</button><a ng-hide="vm.voto.id == null || !vm.isAuthenticated()" ng-click="vm.removerVoto();" data-translate="aondevamosApp.voto.removeVote"></a></div>';
                var compiledContent = $compile(content)($scope);

                GoogleAPI.maps.event.addListener(marker, 'click', (function(marker, content) {
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, compiledContent[0], $scope));
            
            }
            
        });

        function updateBoard() {
            var voto = vm.votos.find(function(voto){
                return voto.id == vm.grupo.id;
            });
            vm.grupo.ranking = voto == null ? [] : voto.ranking;
        };

        function votar(index) {
            
            vm.voto.placeId = vm.places[index].place_id;
            vm.voto.placeName = vm.places[index].name;
            vm.voto.grupo = vm.grupo;
            vm.voto.grupo.ranking = null;
            
            Voto.save(vm.voto, function(result){
                vm.voto = result;
                Voto.count(function(votos){
                    vm.votos = votos;
                    updateBoard();
                });
            });
            
        }
        
        function removerVoto() {
            Voto.delete({id: vm.voto.id}, function(){
                vm.voto = {};
                Voto.count(function(votos){
                    vm.votos = votos;
                    updateBoard();
                });
            });
        }

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.isAuthenticated()) { vm.voto = Voto.getFromUser(); }
            });
        }
        
        function register () {
            $state.go('register');
        }
        
    }
})();
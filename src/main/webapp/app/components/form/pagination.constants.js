(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

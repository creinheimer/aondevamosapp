(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('GrupoJoinController',GrupoJoinController);

    GrupoJoinController.$inject = ['$uibModalInstance', 'entity', '$http'];

    function GrupoJoinController($uibModalInstance, entity, $http) {
        var vm = this;

        vm.grupo = entity;
        vm.clear = clear;
        vm.confirmJoin = confirmJoin;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmJoin (id) {
            $http.put("api/grupos/" + id + "/join").then(function(){
                $uibModalInstance.close(true);
            });
        }
        
    }
})();

(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('GrupoLeftController',GrupoLeftController);

    GrupoLeftController.$inject = ['$uibModalInstance', 'entity', '$http'];

    function GrupoLeftController($uibModalInstance, entity, $http) {
        var vm = this;

        vm.grupo = entity;
        vm.clear = clear;
        vm.confirmLeft = confirmLeft;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmLeft (id) {
            $http.put("api/grupos/" + id + "/left").then(function(){
                $uibModalInstance.close(true);
            });
        }
        
    }
})();

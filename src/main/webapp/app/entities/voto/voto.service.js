(function() {
    'use strict';
    angular
        .module('aondevamosApp')
        .factory('Voto', Voto);

    Voto.$inject = ['$resource', 'DateUtils'];

    function Voto ($resource, DateUtils) {
        var resourceUrl =  'api/votos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dataVoto = DateUtils.convertLocalDateFromServer(data.dataVoto);
                    }
                    return data;
                }
            },
            'getFromUser': {
                method: 'GET',
                url: 'api/votos/my',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dataVoto = DateUtils.convertLocalDateFromServer(data.dataVoto);
                    }
                    return data;
                }
            },
            'count': {
                method: 'GET',
                isArray: true,
                url: 'api/votos/count'
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dataVoto = DateUtils.convertLocalDateToServer(data.dataVoto);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dataVoto = DateUtils.convertLocalDateToServer(data.dataVoto);
                    return angular.toJson(data);
                }
            }
        });
    }
})();

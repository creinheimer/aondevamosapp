(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('VotoDeleteController',VotoDeleteController);

    VotoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Voto'];

    function VotoDeleteController($uibModalInstance, entity, Voto) {
        var vm = this;

        vm.voto = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Voto.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('voto', {
            parent: 'entity',
            url: '/voto?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aondevamosApp.voto.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/voto/votos.html',
                    controller: 'VotoController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('voto');
                    $translatePartialLoader.addPart('tipoRefeicao');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('voto-detail', {
            parent: 'entity',
            url: '/voto/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'aondevamosApp.voto.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/voto/voto-detail.html',
                    controller: 'VotoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('voto');
                    $translatePartialLoader.addPart('tipoRefeicao');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Voto', function($stateParams, Voto) {
                    return Voto.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'voto',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('voto-detail.edit', {
            parent: 'voto-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/voto/voto-dialog.html',
                    controller: 'VotoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Voto', function(Voto) {
                            return Voto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('voto.new', {
            parent: 'voto',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/voto/voto-dialog.html',
                    controller: 'VotoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dataVoto: null,
                                tipoRefeicao: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('voto', null, { reload: 'voto' });
                }, function() {
                    $state.go('voto');
                });
            }]
        })
        .state('voto.edit', {
            parent: 'voto',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/voto/voto-dialog.html',
                    controller: 'VotoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Voto', function(Voto) {
                            return Voto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('voto', null, { reload: 'voto' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('voto.delete', {
            parent: 'voto',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/voto/voto-delete-dialog.html',
                    controller: 'VotoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Voto', function(Voto) {
                            return Voto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('voto', null, { reload: 'voto' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

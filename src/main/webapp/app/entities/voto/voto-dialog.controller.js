(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('VotoDialogController', VotoDialogController);

    VotoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Voto', 'User', 'Grupo'];

    function VotoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Voto, User, Grupo) {
        var vm = this;

        vm.voto = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.grupos = Grupo.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.voto.id !== null) {
                Voto.update(vm.voto, onSaveSuccess, onSaveError);
            } else {
                Voto.save(vm.voto, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('aondevamosApp:votoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dataVoto = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();

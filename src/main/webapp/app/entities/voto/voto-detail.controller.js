(function() {
    'use strict';

    angular
        .module('aondevamosApp')
        .controller('VotoDetailController', VotoDetailController);

    VotoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Voto', 'User', 'Restaurante'];

    function VotoDetailController($scope, $rootScope, $stateParams, previousState, entity, Voto, User, Restaurante) {
        var vm = this;

        vm.voto = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('aondevamosApp:votoUpdate', function(event, result) {
            vm.voto = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

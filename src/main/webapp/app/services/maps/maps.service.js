(function () {
    'use strict';

    angular
        .module('aondevamosApp')
        .factory('Maps', Maps);

    Maps.$inject = ['$window', '$q', '$log', 'DEFAULT_POSITION'];

    function Maps($window, $q, $log, DEFAULT_POSITION){
        
        function location(){
            
            var d = $q.defer();

            if (!$window.navigator.geolocation) {
                $log.info("Geolocation not supported.");
                d.resolve({
                    lat: DEFAULT_POSITION.latitude, 
                    lng: DEFAULT_POSITION.longitude
                }); 
            } else {
                $window.navigator.geolocation.getCurrentPosition(function (position) {
                    d.resolve({
                        lat: position.coords.latitude, 
                        lng: position.coords.longitude
                    }); 
                }, function (error) {
                    switch(error.code) {
                        case error.PERMISSION_DENIED: $log.info("User denied the request for Geolocation."); break;
                        case error.POSITION_UNAVAILABLE: $log.info("Location information is unavailable."); break;
                        case error.TIMEOUT: $log.info("The request to get user location timed out."); break;
                        case error.UNKNOWN_ERROR: $log.info("An unknown error occurred."); break;
                    }
                    d.resolve({
                        lat: DEFAULT_POSITION.latitude, 
                        lng: DEFAULT_POSITION.longitude
                    }); 
                });
            }

            return d.promise;
            
        }
        
        return {
            location: location
        };
        
    }
    
})();

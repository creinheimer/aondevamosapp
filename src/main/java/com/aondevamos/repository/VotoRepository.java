package com.aondevamos.repository;

import com.aondevamos.domain.Grupo;
import com.aondevamos.domain.User;
import com.aondevamos.domain.Voto;
import java.time.LocalDate;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Voto entity.
 */
@SuppressWarnings("unused")
public interface VotoRepository extends JpaRepository<Voto,Long> {

    @Query("select voto from Voto voto where voto.user.login = ?#{principal.username}")
    List<Voto> findByUserIsCurrentUser();
    
    List<Voto> findAllByDataVotoGreaterThanEqual(LocalDate dataVoto);
    
    List<Voto> findAllByGrupoAndDataVotoGreaterThanEqual(Grupo grupo, LocalDate dataVoto);
    
    Voto findByUserAndDataVoto(User user, LocalDate createdDate);

}

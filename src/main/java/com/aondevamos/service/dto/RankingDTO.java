package com.aondevamos.service.dto;

public class RankingDTO {

    private Long quantity;
    private String placeName;

    public RankingDTO(Long quantity, String placeName) {
        this.quantity = quantity;
        this.placeName = placeName;
    }
    
    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
    
}

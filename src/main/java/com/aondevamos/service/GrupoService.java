package com.aondevamos.service;

import com.aondevamos.domain.Grupo;
import com.aondevamos.domain.User;
import com.aondevamos.repository.GrupoRepository;
import com.aondevamos.repository.UserRepository;
import com.aondevamos.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import org.springframework.security.access.AccessDeniedException;

/**
 * Service Implementation for managing Grupo.
 */
@Service
@Transactional
public class GrupoService {

    private final Logger log = LoggerFactory.getLogger(GrupoService.class);
    
    @Inject
    private GrupoRepository grupoRepository;

    @Inject
    private UserService userService;
    
    @Inject
    private UserRepository userRepository;
    
    /**
     * Save a grupo.
     *
     * @param grupo the entity to save
     * @return the persisted entity
     */
    public Grupo save(Grupo grupo) {
        log.debug("Request to save Grupo : {}", grupo);
        if (grupo.getId() != null){
            SecurityUtils.checkUserAuth(grupoRepository.findOne(grupo.getId()));
        }
        Grupo result = grupoRepository.save(grupo);
        return result;
    }

    /**
     *  Get all the grupos.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Grupo> findAll(Pageable pageable) {
        log.debug("Request to get all Grupos");
        Page<Grupo> result = grupoRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one grupo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Grupo findOne(Long id) {
        log.debug("Request to get Grupo : {}", id);
        Grupo grupo = grupoRepository.findOne(id);
        return grupo;
    }

    /**
     *  Delete the  grupo by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Grupo : {}", id);
        SecurityUtils.checkUserAuth(grupoRepository.findOne(id));
        grupoRepository.delete(id);
    }
    
    public Grupo join(Long id) {
        log.debug("Request to join Grupo : {}", id);
        Grupo grupo = grupoRepository.findOne(id);
        User user = userService.getUserWithAuthorities();
        user.getGrupos().add(grupo);
        userRepository.save(user);
        return grupo;
    }
    
    public Grupo left(Long id) {
        log.debug("Request to left Grupo : {}", id);
        Grupo grupo = grupoRepository.findOne(id);
        User user = userService.getUserWithAuthorities();
        user.getGrupos().remove(grupo);
        userRepository.save(user);
        return grupo;
    }
    
}

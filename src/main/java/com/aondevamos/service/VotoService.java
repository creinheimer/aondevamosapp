package com.aondevamos.service;

import com.aondevamos.domain.Grupo;
import com.aondevamos.domain.User;
import com.aondevamos.domain.Voto;
import com.aondevamos.repository.VotoRepository;
import com.aondevamos.security.SecurityUtils;
import com.aondevamos.service.dto.RankingDTO;
import com.aondevamos.web.rest.errors.VotoDuplicadoException;
import com.aondevamos.web.rest.errors.VotoDuplicadoSemanaException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import javax.inject.Inject;

@Service
@Transactional
public class VotoService {

    private final Logger log = LoggerFactory.getLogger(VotoService.class);
    
    @Inject
    private VotoRepository votoRepository;
    
    @Inject
    private UserService userService;

    public Voto save(Voto voto) {
        log.debug("Request to save Voto : {}", voto);
        if (votoExistsInWeek(voto)){
            throw new VotoDuplicadoSemanaException();
        }
        voto.setUser(userService.getUserWithAuthorities());
        try {
            return votoRepository.save(voto);
        } catch (DataIntegrityViolationException e) {
            throw new VotoDuplicadoException(e);
        }
    }
    
    public boolean votoExistsInWeek(Voto voto){
        if (LocalDate.now().getDayOfWeek().equals(DayOfWeek.MONDAY)) return false;
        LocalDate monday = LocalDate.now().with(DayOfWeek.MONDAY);
        List<Voto> votos = votoRepository.findAllByGrupoAndDataVotoGreaterThanEqual(voto.getGrupo(), monday);
        Map<LocalDate, List<Voto>> votoGroup = votos.stream().collect(Collectors.groupingBy(Voto::getDataVoto, Collectors.toList()));
        Set<String> winners = new HashSet<>();
        votoGroup.forEach((k, v) -> { 
            if (!LocalDate.now().equals(k)){
                Map<String, Long> groupPlace = v.stream().collect(Collectors.groupingBy(Voto::getPlaceId, Collectors.counting()));
                winners.add(Collections.max(groupPlace.entrySet(), Map.Entry.comparingByValue()).getKey());
            }
        });
        return winners.stream().anyMatch(v -> v.equals(voto.getPlaceId()));
    }

    @Transactional(readOnly = true) 
    public Page<Voto> findAll(Pageable pageable) {
        log.debug("Request to get all Votos");
        Page<Voto> result = votoRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true) 
    public Voto findOne(Long id) {
        log.debug("Request to get Voto : {}", id);
        Voto voto = votoRepository.findOne(id);
        return voto;
    }
    
    public Voto findUserVote() {
        log.debug("Request to list votos of User");
        User user = userService.getUserWithAuthorities();
        return votoRepository.findByUserAndDataVoto(user, LocalDate.now());
    }

    public void delete(Long id) {
        log.debug("Request to delete Voto : {}", id);
        SecurityUtils.checkUserAuth(votoRepository.findOne(id));
        votoRepository.delete(id);
    }
   
    public Set<Grupo> count() {
        log.debug("Request to votos of day : {}");
        return ranking(LocalDate.now());
    }

    private Set<Grupo> ranking(LocalDate date) {
        List<Voto> votos = votoRepository.findAllByDataVotoGreaterThanEqual(date);
        Map<Grupo, List<Voto>> groupGrupos = votos.stream().collect(Collectors.groupingBy(Voto::getGrupo, Collectors.toList()));
        groupGrupos.forEach((k, v) -> { 
            Map<String, Long> votoGroup = v.stream().collect(Collectors.groupingBy(Voto::getPlaceName, Collectors.counting()));
            votoGroup.forEach((kVoto, vVoto) -> { k.getRanking().add(new RankingDTO(vVoto, kVoto)); });
            Collections.sort(k.getRanking(), (p1, p2) -> p1.getQuantity().compareTo(p2.getQuantity()));
        });
        return groupGrupos.keySet();
    }
    
}

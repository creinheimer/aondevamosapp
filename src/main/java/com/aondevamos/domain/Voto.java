package com.aondevamos.domain;

import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Objects;
import org.springframework.data.annotation.CreatedDate;

/**
 * A Voto.
 */
@Entity
@Table(name = "voto")
public class Voto extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne
    @NotNull
    private Grupo grupo;

    @NotNull
    private String placeId;
    
    @NotNull
    private String placeName;
    
    @CreatedDate
    @Column(nullable = false)
    private LocalDate dataVoto = LocalDate.now();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public Voto user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
    
    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public LocalDate getDataVoto() {
        return dataVoto;
    }

    public void setDataVoto(LocalDate dataVoto) {
        this.dataVoto = dataVoto;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Voto voto = (Voto) o;
        if (voto.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, voto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Voto{"
            + "id=" + id + '}';
    }
}

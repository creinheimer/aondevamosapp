package com.aondevamos.domain;


import com.aondevamos.service.dto.RankingDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Objects;

/**
 * A Grupo.
 */
@Entity
@Table(name = "grupo")
public class Grupo extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Transient
    @JsonProperty
    private List<RankingDTO> ranking = new ArrayList<>();
    
    @Override
    public String getCreatedBy() {
        return super.getCreatedBy(); 
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Grupo nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<RankingDTO> getRanking() {
        return ranking;
    }

    public void setRanking(List<RankingDTO> ranking) {
        this.ranking = ranking;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Grupo grupo = (Grupo) o;
        if(grupo.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, grupo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Grupo{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            '}';
    }
}

package com.aondevamos.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.aondevamos.domain.Voto;
import com.aondevamos.service.VotoService;
import com.aondevamos.web.rest.util.HeaderUtil;
import com.aondevamos.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class VotoResource {

    private final Logger log = LoggerFactory.getLogger(VotoResource.class);
        
    @Inject
    private VotoService votoService;

    @RequestMapping(value = "/votos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Voto> createVoto(@Valid @RequestBody Voto voto) throws URISyntaxException {
        log.debug("REST request to save Voto : {}", voto);
        if (voto.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("voto", "idexists", "A new voto cannot already have an ID")).body(null);
        }
        Voto result = votoService.save(voto);
        return ResponseEntity.created(new URI("/api/votos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("voto", result.getId().toString()))
            .body(result);
    }

    @RequestMapping(value = "/votos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Voto> updateVoto(@Valid @RequestBody Voto voto) throws URISyntaxException {
        log.debug("REST request to update Voto : {}", voto);
        if (voto.getId() == null) {
            return createVoto(voto);
        }
        Voto result = votoService.save(voto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("voto", voto.getId().toString()))
            .body(result);
    }

    @RequestMapping(value = "/votos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Voto>> getAllVotos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Votos");
        Page<Voto> page = votoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/votos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/votos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Voto> getVoto(@PathVariable Long id) {
        log.debug("REST request to get Voto : {}", id);
        Voto voto = votoService.findOne(id);
        return Optional.ofNullable(voto)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/votos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVoto(@PathVariable Long id) {
        log.debug("REST request to delete Voto : {}", id);
        votoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("voto", id.toString())).build();
    }
    
    @RequestMapping(value = "/votos/my",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Voto> findUserVote() {
        log.debug("Request to list votos of User");
        Voto voto = votoService.findUserVote();
        return ResponseEntity.ok(voto);
    }
    
    @RequestMapping(value = "/votos/count",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<?> count() {
        log.debug("Request to list votos of User");
        return ResponseEntity.ok(votoService.count());
    }

}

package com.aondevamos.web.rest.errors;

public class VotoDuplicadoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public VotoDuplicadoException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public VotoDuplicadoException(String message) {
        super(message);
    }

    public VotoDuplicadoException(Throwable cause) {
        super(cause);
    }

}

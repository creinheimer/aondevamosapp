package com.aondevamos.web.rest.errors;

public class VotoDuplicadoSemanaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public VotoDuplicadoSemanaException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public VotoDuplicadoSemanaException(String message) {
        super(message);
    }

    public VotoDuplicadoSemanaException(Throwable cause) {
        super(cause);
    }
    
    public VotoDuplicadoSemanaException() {
        super();
    }

}

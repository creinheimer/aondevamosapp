/**
 * View Models used by Spring MVC REST controllers.
 */
package com.aondevamos.web.rest.vm;
